package ru.t1.fpavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Update user profile";

    @NotNull
    public static final String NAME = "update-profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = this.getAuthService().getUserId();
        @NotNull final String firstName = this.input("First name:");
        @NotNull final String lastName = this.input("Last name:");
        @NotNull final String middleName = this.input("Middle name:");
        this.getServiceLocator().getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

}

package ru.t1.fpavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Logout";

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        this.getAuthService().logout();
    }

}

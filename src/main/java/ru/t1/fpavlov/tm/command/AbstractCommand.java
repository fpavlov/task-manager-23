package ru.t1.fpavlov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.model.ICommand;
import ru.t1.fpavlov.tm.api.service.IServiceLocator;
import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 07.12.2021.
 */
@NoArgsConstructor
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract String getName();

    @NotNull
    public abstract Role[] getRoles();

    @NotNull
    protected final IServiceLocator getServiceLocator() {
        return this.serviceLocator;
    }

    public final void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public final String getUserId() {
        return this.getServiceLocator().getAuthService().getUserId();
    }

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = this.getName();
        @Nullable final String description = this.getDescription();
        @Nullable final String argument = this.getArgument();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}

package ru.t1.fpavlov.tm.command.entity.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove project by index";

    @NotNull
    public static final String NAME = "project-remove-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Project entity = this.findByIndex();
        @NotNull final String userId = this.getUserId();
        this.getProjectTaskService().removeProject(userId, entity);
    }

}

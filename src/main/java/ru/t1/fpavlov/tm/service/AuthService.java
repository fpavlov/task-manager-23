package ru.t1.fpavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.service.IAuthService;
import ru.t1.fpavlov.tm.api.service.IUserService;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.exception.field.PasswordEmptyException;
import ru.t1.fpavlov.tm.exception.user.*;
import ru.t1.fpavlov.tm.model.User;
import ru.t1.fpavlov.tm.util.HashUtil;

import java.util.Arrays;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    @NotNull
    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (email == null || email.isEmpty()) {
            return this.userService.create(login, password);
        }
        return this.userService.create(login, password, email);
    }

    @Override
    public void login(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = this.userService.findByLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (user == null || passwordHash == null || !passwordHash.equals(user.getPasswordHash())) {
            throw new IncorrectLoginOrPasswordException();
        }
        if (user.getIsLocked()) throw new AuthenticationException();
        this.userId = user.getId();
    }

    @Override
    public void logout() {
        this.userId = null;
    }

    @Override
    public boolean isAuth() {
        return this.userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!this.isAuth()) throw new AccessDeniedException();
        return this.userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!this.isAuth()) throw new AccessDeniedException();
        @Nullable  final User user = this.userService.findById(this.userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(@Nullable Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = this.getUser();
        @NotNull final Role userRole = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(userRole);
        if (!hasRole) throw new PermissionException();
    }

}

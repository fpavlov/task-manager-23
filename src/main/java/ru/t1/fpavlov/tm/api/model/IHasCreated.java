package ru.t1.fpavlov.tm.api.model;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IHasCreated {

    @Nullable
    Date getCreated();

    void setCreated(@Nullable final Date created);

}

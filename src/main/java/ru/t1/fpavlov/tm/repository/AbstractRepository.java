package ru.t1.fpavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.IRepository;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.AbstractModel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 21.12.2021.
 */
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> items = new ArrayList<>();

    @Override
    public void clear() {
        this.items.clear();
    }

    @Override
    public int getSize() {
        return this.items.size();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return new ArrayList<>(this.items);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        @NotNull final List<M> items = this.findAll();
        items.sort(comparator);
        return items;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return this.findAll();
        return findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M add(@Nullable final M item) {
        this.items.add(item);
        return item;
    }

    @NotNull
    private String calculateGetterName(@NotNull final String fieldName) {
        @NotNull String getterName = "get";
        @NotNull final String nameParts[] = fieldName.toLowerCase().split(" ");
        for (final String part : nameParts) {
            getterName += Character.toUpperCase(part.charAt(0)) + part.substring(1);
        }

        return getterName;
    }

    @Nullable
    protected M findByTextField(@Nullable final String fieldName, @Nullable final String searchValue) {
        if (fieldName == null || fieldName.isEmpty()) return null;
        if (searchValue == null || searchValue.isEmpty()) return null;
        return this.items
                .stream()
                .filter(m -> searchValue.equals(this.getValueByFieldName(m, fieldName)))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    private String getValueByFieldName(@Nullable final M item, @NotNull final String fieldName) {
        final Class<M> clazz =
                (Class<M>) ((ParameterizedType) getClass().getGenericSuperclass())
                        .getActualTypeArguments()[0];

        @NotNull final String methodName = this.calculateGetterName(fieldName);
        @Nullable String fieldValue;

        try {
            @NotNull final Method method = clazz.getMethod(methodName);
            fieldValue = (String) method.invoke(item);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            fieldValue = null;
        }

        return fieldValue;
    }

    @Nullable
    @Override
    public M findById(@Nullable final String id) {
        return this.findByTextField(M.ID_FIELD_NAME, id);
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final Integer index) {
        return this.items.get(index);
    }

    @Nullable
    @Override
    public M remove(@Nullable final M item) {
        this.items.remove(item);
        return item;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        @Nullable final M item = this.findById(id);
        if (item == null) return null;
        return this.remove(item);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        @Nullable final M item = this.findByIndex(index);
        if (item == null) return null;
        return this.remove(item);
    }

    @Override
    public boolean isIdExist(@Nullable final String id) {
        return this.findById(id) != null;
    }

}

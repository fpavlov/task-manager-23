package ru.t1.fpavlov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.comparator.CreatedComparator;
import ru.t1.fpavlov.tm.comparator.NameComparator;
import ru.t1.fpavlov.tm.comparator.StatusComparator;

import java.util.Comparator;

/**
 * Created by fpavlov on 26.11.2021.
 */
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    @NotNull Sort(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return this.displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return this.comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort item : values()) {
            if (value.equals(item.getDisplayName())) return item;
        }
        return null;
    }

    @NotNull
    public static String[] displayValues() {
        @NotNull final Sort sortNames[] = Sort.values();
        @NotNull final String sortValues[] = new String[sortNames.length];
        for (int i = 0; i < sortValues.length; i++) {
            sortValues[i] = sortNames[i].getDisplayName();
        }
        return sortValues;
    }

}

package ru.t1.fpavlov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class CommandIncorrectException extends AbstractSystemException {

    @NotNull
    public CommandIncorrectException() {
        super("Error! Incorrect command.");
    }

    @NotNull
    public CommandIncorrectException(@Nullable final String command) {
        super("Error! Command " + command + " is incorrect.");
    }

}

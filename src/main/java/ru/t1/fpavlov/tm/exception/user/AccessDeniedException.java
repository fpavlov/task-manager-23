package ru.t1.fpavlov.tm.exception.user;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class AccessDeniedException extends AbstractUserException {

    @NotNull
    public AccessDeniedException() {
        super("Error! Access denied. Login and try again");
    }

}

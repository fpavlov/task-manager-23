package ru.t1.fpavlov.tm.exception.field;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 27.01.2022.
 */
public class StatusEmptyException extends AbstractFieldException {

    @NotNull
    public StatusEmptyException() {
        super("Error! Status is required!");
    }

}
